package com.juliusdotsh.repositories;

import com.juliusdotsh.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	
	User findByTelegramId(@Param("telegramId")long telegramId);

	int countByTelegramId(@Param("telegramId") long telegramId);

}
