package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class Start implements Command {

	private JarvisBotService jarvisBotService;
	private UserRepository userRepository;

	@Autowired
	public Start(JarvisBotService jarvisBotService, UserRepository userRepository) {
		this.userRepository = userRepository;
		this.jarvisBotService = jarvisBotService;
	}

	@Override
	@Transactional
	public void onExec(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		jarvisBotService.sendMessageHome(message.getChatId(), LanguageManager.getLabel(user, "message.start"), user);
		if (userRepository.countByTelegramId(message.getChatId()) == 0) {
			jarvisBotService.sendMessage(message.getChatId(), LanguageManager.getLabel(user, "message.command.start"));
			User userToSave = new User();
			userToSave.setTelegramId(message.getChatId());
			userToSave.setState(0);
			userToSave.setCron(true);
			userToSave.setLanguage("en");
			userRepository.save(userToSave);
		}
	}

}
