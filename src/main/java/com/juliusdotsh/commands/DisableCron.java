package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import com.juliusdotsh.utility.KeyboardManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;

@Component
public class DisableCron implements Command {

	private UserRepository userRepository;
	private JarvisBotService jarvisBotService;

	@Autowired
	public DisableCron(UserRepository userRepository, JarvisBotService jarvisBotService) {
		this.userRepository = userRepository;
		this.jarvisBotService = jarvisBotService;
	}

	@Override
	@Transactional
	public void onExec(Message message) {
		User bad = userRepository.findByTelegramId(message.getChatId());
		bad.setCron(false);
		userRepository.save(bad);
		SendMessage sendMessage = new SendMessage()
				.setReplyMarkup(KeyboardManager.getSettingsKeyboard(bad))
				.enableMarkdown(true)
				.setChatId(bad.getTelegramId())
				.setText(LanguageManager.getLabel(bad, "message.cron.disabling"));
		jarvisBotService.execute(sendMessage);
	}
}
