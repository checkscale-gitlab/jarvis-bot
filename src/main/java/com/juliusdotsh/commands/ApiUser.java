package com.juliusdotsh.commands;

import com.juliusdotsh.components.LanguageManager;
import com.juliusdotsh.entities.User;
import com.juliusdotsh.interfaces.Command;
import com.juliusdotsh.repositories.UserRepository;
import com.juliusdotsh.services.JarvisBotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Message;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ApiUser implements Command {

	private JarvisBotService jarvisBotService;
	private UserRepository userRepository;

	@Autowired
	public ApiUser(JarvisBotService jarvisBotService, UserRepository userRepository) {
		this.jarvisBotService = jarvisBotService;
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public void onExec(Message message) {
		User user = userRepository.findByTelegramId(message.getChatId());
		jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "message.command.api.user"));
		Pattern patternForApiUser = Pattern.compile("^[0-z]{8}-[0-z]{4}-[0-z]{4}-[0-z]{4}-[0-z]{12}$");
		Matcher matcherForApiUser = patternForApiUser.matcher(message.getText());
		if (matcherForApiUser.find()) {
			user.setState(2);
			user.setApiUser(message.getText().trim());
			userRepository.save(user);
		} else {
			jarvisBotService.sendMessage(user.getTelegramId(), LanguageManager.getLabel(user, "error.invalid.pattern"));
		}
	}
}
