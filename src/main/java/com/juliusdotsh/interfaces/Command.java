package com.juliusdotsh.interfaces;

import org.telegram.telegrambots.meta.api.objects.Message;

public interface Command {

	void onExec(Message message);
}
