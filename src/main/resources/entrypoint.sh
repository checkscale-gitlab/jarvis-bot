#!/usr/bin/env bash

if [! -z $JARVIS_BOT_NAME && ! -z $JARVIS_BOT_TOKEN]; then
    java -jar /usr/local/bin/telegramapp.jar -Djarvis.botname=$JARVIS_BOT_NAME -Djarvis.bottoken=$JARVIS_BOT_TOKEN
else
    >&2 echo "Error launching Jarvis - bot variables haven't been initialized "
fi